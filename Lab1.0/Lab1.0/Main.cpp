#include <iostream>
#include "DynamicArrHead.h"

using namespace std;

int main() {
	DynamicArrayOfIntegers one(3, 1);
	DynamicArrayOfIntegers two(2, 2);
	cout << "First more second? (1-Yes,0-No) " << (one > two) << endl;
	one = two;
	cout << "First array " << one << endl;
	cout << "Zero elem first array " << one.getElem(0) << endl;
	DynamicArrayOfIntegers three(2, 1);
	DynamicArrayOfIntegers five(3, 2);
	cout << "Third less fifth? (1-Yes,0-No) " << (three < five) << endl;
	three = five;
	cout << "Third array " << three << endl;
	DynamicArrayOfIntegers seven(3, 2);
	DynamicArrayOfIntegers eight(3, 2);
	cout << "Seventh equal eighth? (1-Yes,0-No) " << (seven == eight) << endl;
	cout << "Seventh array" << seven << endl;
	DynamicArrayOfIntegers nine(2);
	cout << "Input array elements " << endl;
	cin >> nine;
	cout << "Introduced array " << nine << endl;
	DynamicArrayOfIntegers ten(2, 5);
	DynamicArrayOfIntegers eleven(2, 5);
	cout << "Eleventh not equal tenth? (1-Yes,0-No) " << (eleven != ten) << endl;
	cout << "Addition of arrays " << eleven + ten << endl;
	DynamicArrayOfIntegers four(2, 5);
	four.resize(6);
	cout << "Output a new four array size " << four.getLength() << endl; //����� ������ ������� ������� four

	return 0;
}