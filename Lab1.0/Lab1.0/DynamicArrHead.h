#pragma once
#ifndef NUMBER1_DYNAMICARRAYOFINTEGERS_H
#define NUMBER1_DYNAMICARRAYOFINTEGERS_H

#include <ostream>

using namespace std;
class DynamicArrayOfIntegers {
private:
	int *array;
	int size;

public:
	DynamicArrayOfIntegers();//����������� �� ���������
	DynamicArrayOfIntegers(int size);//����������� �� �������
	DynamicArrayOfIntegers(int size, int n);//�����������  �� ������� � ����� n
	DynamicArrayOfIntegers(const DynamicArrayOfIntegers& copy);//����������� �����������
	DynamicArrayOfIntegers(DynamicArrayOfIntegers&& temp);//����������� �����������
	~DynamicArrayOfIntegers();//����������

	int getLength();//����� �������
	int operator [](int x);//������ � ��������
	bool resize(int newSize);//��������� �������
	DynamicArrayOfIntegers operator =(const DynamicArrayOfIntegers&  m1);//�������� ������������
	DynamicArrayOfIntegers operator =(DynamicArrayOfIntegers&&  m1);//�������� �����������

	bool operator==(const DynamicArrayOfIntegers &m1) const;//�������� ==
	bool operator!=(const DynamicArrayOfIntegers &m1) const;//�������� !=

	bool operator >(DynamicArrayOfIntegers const &m1);//�������� >
	bool operator <(DynamicArrayOfIntegers const &m1);//�������� <
	bool operator >=(DynamicArrayOfIntegers const &m1);//�������� >=
	bool operator <=(DynamicArrayOfIntegers const &m1);//�������� <=

	DynamicArrayOfIntegers& operator + (const DynamicArrayOfIntegers& m2);//�������� ��������

	friend std::ostream &operator<<(std::ostream &, const DynamicArrayOfIntegers  &);//�������� ������
	friend std::istream &operator>>(std::istream &in, DynamicArrayOfIntegers &);//�������� �����

	void toString();
	int getElem(int i);
};


#endif //NUMBER1_DYNAMICARRAYOFINTEGERS_H