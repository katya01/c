#include "TreeDictionaryHead.h"
#include <iostream>

int main() {
	TreeDictionary* tree = new TreeDictionary();
	tree->addWord("Hello");
	tree->addWord("World");
	tree->addWord("Anya");
	tree->addWord("Anya");
	tree->addWord("Cookies");
	tree->addWord("Easy");
	tree->addWord("Idea");
	tree->addWord("Denis");
	tree->addWord("Alex");
	tree->addWord("Anya");
	tree->addWord("Alex");
	tree->addWord("Ghost");
	tree->addWord("Cookies");
	tree->addWord("Rat");
	tree->addWord("James");
	tree->addWord("Easy");
	tree->addWord("Rat");
	tree->addWord("Idea");
	tree->addWord("Anya");
	tree->addWord("Anya");
	std::cout << *tree << std::endl;
	std::cout << "count words in tree:" << tree->countWords() << std::endl;
	std::cout << "frequency word \"Anya\" :" << tree->findWord("Anya") << std::endl;
	std::cout << "frequency word \"Ghost\" :" << tree->findWord("Ghost") << std::endl;
	std::cout << "frequency word \"Timmy\" :" << tree->findWord("Timmy") << std::endl;
	std::cout << "delete words" << std::endl;
	tree->deleteWord("Claw");
	tree->deleteWord("Bottle");
	tree->deleteWord("Anya");
	tree->deleteWord("Ghost");
	std::cout << *tree << std::endl;
	std::cout << "count word: " << tree->countWords() << std::endl;
	std::cout << "frequency word \"Anya\" :" << tree->findWord("Anya") << std::endl;
	std::cout << "frequency word \"Ghost\" :" << tree->findWord("Ghost") << std::endl;
	std::cout << "frequency word \"Timmy\" :" << tree->findWord("Timmy") << std::endl;
	delete tree;
	return 0;
}