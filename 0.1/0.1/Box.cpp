#include "BoxHead.h"
#include <iostream>

using namespace std;
namespace lp {
	Box::Box(int leng, int wid, int heig, double weig, int val) {
		length = leng; //�����
		width = wid; //������
		height = heig; //������
		weight = weig; //��
		value = val; //���������
	}

	int Box::getLength() //�������
	{
		return length;
	}

	void Box::setLength(int length) //�������
	{
		Box::length = length;
	}

	int Box::getWidth() //*
	{
		return width;
	}

	void Box::setWidth(int width) //*
	{
		Box::width = width;
	}

	int Box::getHeight() //*
	{
		return height;
	}

	void Box::setHeight(int height) //* 
	{
		Box::height = height;
	}

	double Box::getWeight() //*
	{
		return weight;
	}

	void Box::setWeight(double weight) //*
	{
		Box::weight = weight;
	}

	int Box::getValue() //* 
	{
		return value;
	}

	void Box::setValue(int value) //*
	{
		Box::value = value;
	}


	bool Box::operator==(const Box &rhs) const //�������� ���������
	{
		return length == rhs.length &&
			width == rhs.width &&
			height == rhs.height &&
			(abs(weight - rhs.weight) < 0.00001) &&
			value == rhs.value;
	}

	bool Box::operator!=(const Box &rhs) const //*
	{
		return !(rhs == *this);
	}


	ostream &operator<<(ostream &os, const Box &box) //�������� ������
	{
		os << "length: " << box.length << " width: " << box.width << " height: " << box.height << " weight: "
			<< box.weight
			<< " value: " << box.value;
		return os;
	}

	istream &operator>>(istream &is, Box &box) //�������� �����
	{
		cout << "Input length:" << endl;
		is >> box.length;
		cout << "Input width:" << endl;
		is >> box.width;
		cout << "Input height:" << endl;
		is >> box.height;
		cout << "Input weight:" << endl;
		is >> box.weight;
		cout << "Input value:" << endl;
		is >> box.value;

		return is;
	}
}