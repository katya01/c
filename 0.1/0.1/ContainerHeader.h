#pragma once
#ifndef NUMBER0_1_CONTAINER_H
#define NUMBER0_1_CONTAINER_H
#include <vector>
#include <iostream>
#include "BoxHead.h"

using namespace std;
namespace lp {
	class Container {
	private:
		std::vector<Box> vector;
		int length;
		int width;
		int height;
		double weight;
		int value;

	public:
		Container(int length, int width, int height, double weight, int value);

		std::vector<Box> &getVector();

		void setVector(std::vector<Box> &vector);

		int getLength();

		void setLength(int length);

		int getWidth();

		void setWidth(int width);

		int getHeight();

		void setHeight(int height);

		double getWeight();

		void setWeight(double weight);

		int getValue();

		void setValue(int value);

		Box getBoxByIndex(int index);

		int count();

		int countValue();

		double countWeight();

		int addContainer(Box box);

		void clearElem(int index);

		Box operator[](int rhs);

		friend std::ostream &operator<<(std::ostream &, const Container &);

		friend std::istream &operator>>(std::istream &in, Container &);

		bool operator==(const Container &rhs) const;

		bool operator!=(const Container &rhs) const;


	};
}
#endif //NUMBER0_1_CONTAINER_H

