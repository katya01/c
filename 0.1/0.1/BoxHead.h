#pragma once
#ifndef NUMBER0_1_BOX_H
#define NUMBER0_1_BOX_H


#include <ostream>

namespace lp {
	class Box {
	private:
		int length; //�����
		int width; //������
		int height; //������
		double weight; //��
		int value; //���������


	public:
		Box(int length, int wid, int heig, double weig, int val);


		bool operator==(const Box &rhs) const; //�������� ���������

		bool operator!=(const Box &rhs) const; //*

		friend std::istream &operator>>(std::istream &is, Box &box); //����

		friend std::ostream &operator<<(std::ostream &os, const Box &box); //�����

		int getLength();

		void setLength(int length);

		int getWidth();

		void setWidth(int width);

		int getHeight();

		void setHeight(int height);

		double getWeight();

		void setWeight(double weight);

		int getValue();

		void setValue(int value);

	};
}
#endif //NUMBER0_BOX_H
