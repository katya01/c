#pragma once
#ifndef NUMBER2_RINGBUFFER_H
#define NUMBER2_RINGBUFFER_H
struct SizeException {};
struct EmptyException {};
class RingBuffer {
private:
	double *arr;
	int size;
	int firstPointer;
	int lastPointer;
	bool flag;
public:
	friend class Iterator;
	RingBuffer(int size);//����������
	void addElement(double element);//�������� ������� � ����� ������� (��� ������������ � ����������)
	double getElem();//����� ������� � ������ ������� (��� ���������� � ����������)
	double seeElem();//�������� ������� � ������ ������� (��� ��� �������)
	int getSize();//������ �������
	void doEmptyQueue();//������� ������� ������
	bool checkEmpty();//�������� ������� �� �������
};

#endif //NUMBER2_RINGBUFFER_H