#pragma once
#ifndef NUMBER2_ITERATOR_H
#define NUMBER2_ITERATOR_H

#include "RingBHead.h"

class Iterator {
private:
	int index;
	RingBuffer *queue;
public:
	friend class RingBuffer;
	Iterator(RingBuffer* &que);//�����������, ������������� �������� � �������
	void start();//������ ������� ���������
	void next();//������� � ���������� ��������
	bool finish();//��������, ��� �� ��������������
	int getValue();//�������� ��������� ������� �������
};


#endif //NUMBER2_ITERATOR_H
