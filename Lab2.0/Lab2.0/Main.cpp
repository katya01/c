#include "IterHead.h"
#include "RingBHead.h"
#include <iostream>
using namespace std;
int main() {
	RingBuffer *ringB = new RingBuffer(5);
	ringB->addElement(1);
	ringB->addElement(2);
	ringB->addElement(3);

	RingBuffer *ringC = new RingBuffer(2);
	ringB->addElement(0);
	if (!ringB->checkEmpty()) {
		cout << "Queue isn't empty" << endl;
	}
	if (ringC->checkEmpty()) {
		cout << "Queue is empty!" << endl;
	}
	Iterator iteratorA = Iterator(ringC);
	Iterator iteratorB = Iterator(ringB);
	while (!iteratorB.finish()) {
		iteratorB.next();
		cout << iteratorB.getValue() << endl;

	}

	cout << "______________________________________" << endl;

	while (!iteratorA.finish()) {
		iteratorA.next();
		cout << iteratorA.getValue() << endl;

	}

	cout << "______________________________________" << endl;
	cout << "Size ringB : " << ringB->getSize() << endl;
	ringB->doEmptyQueue();
	if (ringB->checkEmpty()) {
		cout << "RingB is empty " << endl;
	}
	iteratorA.start();
	while (!iteratorA.finish()) {
		iteratorA.next();
		cout << iteratorA.getValue() << endl;

	}


	delete ringB;
	delete ringC;

	return 0;
}
