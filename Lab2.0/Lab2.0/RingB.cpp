#include "RingBHead.h"

RingBuffer::RingBuffer(int size) //�����������
{
	flag = true;
	arr = new double[size];
	this->size = size;
	firstPointer = 0;
	lastPointer = 0;

}


void RingBuffer::addElement(double element) //�������� ������� � ����� ������� (��� ������������ � ����������)
{
	if ((lastPointer + 1) % size == firstPointer) {
		throw SizeException();
	}
	if (flag) {
		arr[lastPointer] = element;
		flag = false;

	}
	else {
		lastPointer++;
		lastPointer = lastPointer % size;
		arr[lastPointer] = element;
	}
}


double RingBuffer::getElem() //����� ������� � ������ ������� (��� ���������� � ����������)
{
	if (flag) {
		throw EmptyException();
	}
	if (firstPointer == lastPointer) {
		flag = true;
		return arr[firstPointer];
	}
	if (firstPointer != lastPointer) {
		firstPointer++;
		return arr[firstPointer - 1];
	}
}


double RingBuffer::seeElem() //�������� ������� � ������ ������� (��� ��� �������)
{
	if (flag) {
		throw EmptyException();
	}
	return arr[firstPointer];
}


int RingBuffer::getSize() //������ �������
{
	return size;
}


void RingBuffer::doEmptyQueue() //������� ������� ������
{
	firstPointer = lastPointer;
	flag = true;

}


bool RingBuffer::checkEmpty() //�������� ������� �� �������
{
	if (flag) { return true; }
	return false;
}